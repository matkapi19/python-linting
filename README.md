# Python Linting

## originaly cloned from

Python linting Docker image used in the pipeline [gitlab-ci-pipeline](https://gitlab.com/mafda/gitlab-ci-pipeline).

> Check the complete post on [Medium](https://medium.com/semantixbr/how-to-make-your-code-shine-with-gitlab-ci-pipelines-48ade99192d1)

--- 

made with 💙 by [mafda](https://mafda.github.io/).
