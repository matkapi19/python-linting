FROM python:3.11.2-alpine3.17
WORKDIR /app
COPY requirements.txt /app
RUN apk add --virtual build-dependencies build-base git && \
    pip install --no-cache-dir -r requirements.txt && \
    apk --no-cache del build-dependencies
